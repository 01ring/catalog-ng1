import { Errors } from './errors.response';
import { Signup } from './signup.response';

export interface Access {
  result: string;
  data: Signup | Errors | {token: string};
}
