import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';
import { Signup } from './signup.response';
import { Errors } from './errors.response';
import { Access } from './access.response';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root',
})
export class BackendService {
  user = {};

  constructor(private http: HttpClient, private messageService: MessageService) {}

  signup(user): Observable<Access> {
    return (
      this.http
        .post<Signup | Errors>('http://localhost:3000/users', user)
        .pipe(
          map((response: any) => {
            const parsed: Access = {
              result: '',
              data: null
            };
            if (response.errors) {
              parsed.result = 'ERROR';
              parsed.data = response.errors;
              this.messageService.add(`${parsed.result}: ${parsed.data}`);
            } else {
              parsed.result = 'OK';
              parsed.data = response;
              this.messageService.add(`${parsed.result}: Signup success, proceed to Login.`);
            }
            return parsed;
          }),
          catchError(this.handleError)
        )
    );
  }

  login(accessForm): Observable<Access> {
    return (
      this.http
        .post<{token: string} | Errors>('http://localhost:3000/api/v1/auth', accessForm)
        .pipe(
          map((response: any) => {
            const parsed: Access = {
              result: '',
              data: null
            };
            if (response.errors) {
              parsed.result = 'ERROR';
              parsed.data = response.errors;
            } else {
              parsed.result = 'OK';
              parsed.data = response;
            }
            this.messageService.add(JSON.stringify(parsed));
            return parsed;
          }),
          catchError(this.handleError)
        )
    );
  }

  clearCart() {}

  private handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }
}
