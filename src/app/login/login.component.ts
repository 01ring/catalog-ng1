import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { Access } from '../access.response';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  signupResponse: Access;
  accessForm = this.formBuilder.group({
    email: '',
    password: '',
  });

  constructor(
    private formBuilder: FormBuilder,
    private backendService: BackendService
  ) {}

  ngOnInit(): void {}

  signup(accessForm): void {
    // Process signup data here
    this.backendService.signup(accessForm).subscribe((parsed: any) => {
      this.signupResponse = parsed
    });
    this.accessForm.reset();
    // this.router.navigate(['/']);
  }

  login(accessForm): void {
    // Process signup data here
    this.backendService.login(accessForm).subscribe((parsed: any) => {
      this.signupResponse = parsed
    });
    this.accessForm.reset();
    // this.router.navigate(['/']);
  }
}
