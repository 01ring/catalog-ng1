export interface Signup {
  id: number;
  email: string;
  created_at: string;
  updated_at: string;
  admin: boolean;
}
